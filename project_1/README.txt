What you need to run:

Code: https://gitlab.com/aaronsilva/7641-ml

Heart Failure Dataset:
https://archive.ics.uci.edu/ml/datasets/Heart+failure+clinical+records#

preprocessing steps for Heart Failure Dataset:
none

Breast Cancer Wisconsin Dataset:
https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Diagnostic%29

preprocessing steps for Breast Cancer Dataset:
- Added column names to the breast cancer dataset extrapolated from the `.names` file
- Removed the `patient ID` column

Environment:
Using Conda for python package management, using python 3.8

to install my environment:
conda env create -f environment.yml

Python library installation:
specific libraries that I installed for ML algorithms and data manipulation were:
- scikit-learn
- pandas
