import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_validate
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, classification_report
import matplotlib.pyplot as plt


def decision_tree(data, class_attr, name, visualize):
    X = data.drop(class_attr, axis=1)
    y = data[class_attr]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    attributes_len = X.shape[1]

    splitters = ['best', 'random']

    for splitter in splitters:
        train_errors = []
        test_errors = []
        for i in range(1, attributes_len + 1):
            classifier = DecisionTreeClassifier(max_depth=i, splitter=splitter)
            classifier.fit(X_train, y_train)
            y_train_pred = classifier.predict(X_train)
            y_test_pred = classifier.predict(X_test)

            train_error = 1 - accuracy_score(y_train, y_train_pred)
            test_error = 1 - accuracy_score(y_test, y_test_pred)

            train_errors.append(train_error)
            test_errors.append(test_error)

            print('DT Depth ', i)
            print('training error: ', train_error)
            print('testing error: ', test_error)

        plt.figure()
        plt.plot(train_errors)
        plt.plot(test_errors)
        plt.title('Decision Tree (splitter: {0}): {1}'.format(splitter, name))
        plt.ylabel('Error')
        plt.xlabel('Depth')
        plt.legend(['Train', 'Test'], loc='upper right')
        plt.xticks(np.arange(len(train_errors)), np.arange(1, len(train_errors)+1))
        plt.savefig("output/DT {0} - {1}".format(splitter, name))
        plt.show(block=visualize)


def neural_networks(data, class_attr, name):
    solvers = ['lbfgs', 'sgd', 'adam']
    iterations = [5, 10, 50, 100, 200, 300, 500]
    for solver in solvers:
        means = []
        for iteration in iterations:
            print('SOLVER: {}, iteration: {}'.format(solver, iteration))
            classifier = MLPClassifier(solver=solver, max_iter=iteration)
            mean_score = run_prediction(classifier, data, class_attr, run_report=False)
            means.append(1 - mean_score)
        plt.figure()
        plt.plot(iterations, means)
        plt.title('{} NN (solver: {}) over iterations'.format(name, solver))
        plt.ylabel('Error')
        plt.xlabel('Iterations')
        plt.savefig("output/NN {0} - {1}".format(name, solver))
        plt.show(block=False)


def boosting(data, class_attr, name):
    estimators = [5, 10, 25, 50, 100, 200]
    for estimator in estimators:
        means = []
        for depth in range(1, data.shape[1]):
            print('estimator: {}, depth: {}'.format(estimator, depth))
            classifier = AdaBoostClassifier(
                n_estimators=estimator,
                base_estimator=DecisionTreeClassifier(max_depth=depth))
            mean_score = run_prediction(classifier, data, class_attr, run_report=False)
            means.append(1 - mean_score)
        plt.figure()
        plt.plot(range(1, data.shape[1]), means)
        plt.title('{} Boosting (estimators: {}), pruning depth'.format(name, estimator))
        plt.ylabel('Error')
        plt.xlabel('Depth')
        plt.savefig("output/Boosting {0} - {1}".format(name, estimator))
        plt.show(block=False)


def support_vector_machines(data, class_attr, name):
    kernels = ['rbf', 'poly']

    for kernel in kernels:
        print('KERNEL: ', kernel)
        classifier = SVC(kernel=kernel)
        mean_score = run_prediction(classifier, data, class_attr)
        print('error ', 1 - mean_score)


def k_nearest_neighbor(data, class_attr, name):
    means = []
    for i in range(1, 10):
        print('nearest neighbor - ', i)
        classifier = KNeighborsClassifier(n_neighbors=i)
        mean_score = run_prediction(classifier, data, class_attr)
        means.append(1 - mean_score)
    plt.figure()
    plt.plot(range(1, 10), means)
    plt.title('{} K Nearest Neighbor: varying K'.format(name))
    plt.ylabel('Error')
    plt.xlabel('K')
    plt.savefig("output/KNN {0}".format(name))
    plt.show(block=False)


def run_prediction(classifier, data, class_attr, run_report=True):
    X = data.drop(class_attr, axis=1)
    y = data[class_attr]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    classifier.fit(X_train, y_train)
    y_train_pred = classifier.predict(X_train)
    y_test_pred = classifier.predict(X_test)

    cross_score = cross_validate(classifier, X, y, cv=5)
    print('Cross validation fit time: ', cross_score['fit_time'])
    print('Cross validation scores: ', cross_score['test_score'])
    print('Cross validation mean score: ', np.mean(cross_score['test_score']))
    print('Training accuracy: ', accuracy_score(y_train, y_train_pred))
    print('Testing accuracy: ', accuracy_score(y_test, y_test_pred))
    if run_report:
        print('Classification report:\n', classification_report(y_test, y_test_pred))

    return np.mean(cross_score['test_score'])


def run_dataset(dataset, name, class_attr, visualize):
    print('===================DECISION TREE=========================')
    decision_tree(dataset, class_attr=class_attr, name=name, visualize=visualize)

    print('===================NEURAL NETWORK========================')
    # no need to visualize, pycharm displays plots on SciView dashboard
    neural_networks(dataset, class_attr=class_attr, name=name)

    print('===================BOOSTING==============================')
    boosting(dataset, class_attr=class_attr, name=name)

    print('===================SUPPORT VECTOR MACHINES===============')
    support_vector_machines(dataset, class_attr=class_attr, name=name)

    print('===================K-NEAREST NEIGHBOR====================')
    k_nearest_neighbor(dataset, class_attr=class_attr, name=name)


def main():
    heart_failure_data = pd.read_csv('heart_failure_clinical_records_dataset.csv', index_col=0)
    breast_cancer_data = pd.read_csv('breast-cancer-wisconsin.csv', index_col=0)

    print('++++++++++++++++++++++++++Heart Failure Dataset++++++++++++++++++++++++++')
    run_dataset(heart_failure_data, class_attr='DEATH_EVENT', name='Heart Failure', visualize=False)

    print('\n\n\n\n++++++++++++++++++++++++++Breast Cancer Dataset++++++++++++++++++++++++++')
    run_dataset(breast_cancer_data, class_attr='Class', name='Breast Cancer', visualize=False)


if __name__ == "__main__":
    main()

